//
//  HostingController.swift
//  PostureFix WatchKit Extension
//
//  Created by Dario Mandarino on 23/01/2020.
//  Copyright © 2020 Dario Mandarino. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView(flag: 2)
    }
}
