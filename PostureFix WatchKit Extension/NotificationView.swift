//
//  NotificationView.swift
//  PostureFix WatchKit Extension
//
//  Created by Dario Mandarino on 23/01/2020.
//  Copyright © 2020 Dario Mandarino. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
