//
//  ContentView.swift
//  timer WatchKit Extension
//
//  Created by Christian on 17/01/2020.
//  Copyright © 2020 Christian. All rights reserved.
//

//
//  ContentView.swift
//  timer WatchKit Extension
//
//  Created by Christian on 17/01/2020.
//  Copyright © 2020 Christian. All rights reserved.
//

import SwiftUI
import WatchKit
import UserNotifications

struct ContentView: View {
    
    let timer = Timer.publish(every: 1, on: .main, in: .common)
    @State var alert = false
    @State var flag : Int
    @State var boolean = true
    @State var appoggioM = 0
    @State var mainText = "How often would you like to be reminded to fix your posture?"
    @State var flagg = false
    
    @State var secondM = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24" ,"25" ,"26" ,"27" ,"28" ,"29", "30"," 31", "32","33"," 34","35" ,"36" ,"37" ,"38" ,"39" ,"40" ,"41" ,"42" ,"43" ,"44" ,"45", "46", "47", "48", "49", "50", "51", "52" ,"53" ,"54" ,"55" ,"56", "57", "58", "59"]
    
    @State  var selectedM = 30
    @State var appoggioH = 0
    
    @State var secondH = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    @State  var selectedH = 0
    var body: some View {
        VStack {
            
            VStack {
                
                VStack {
                    
                    VStack {
                        
                        Text(mainText).onReceive(self.timer){_ in
                            if self.flag == 0
                            {
                                self.boolean = false
                                if self.selectedM == 0
                                {
                                    if self.selectedH == 0
                                    {
                                        UNUserNotificationCenter.current()
                                            .requestAuthorization(options: [.alert,.sound,.badge ])
                                            { (status, _) in
                                                
                                                let content = UNMutableNotificationContent()
                                                content.title = "PostureFix"
                                                content.body = "Good posture breathes confidence"
                                                
                                                //Time remaining until next reminder.
                                                // this time interval represents the delay time of notification
                                                // ie., the notification will be delivered after the delay.....
                                                
                                                let request = UNNotificationRequest(identifier: "noti", content: content, trigger: .none)
                                                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                                                
                                                
                                                //self.alert.toggle()
                                                
                                                self.selectedM = self.appoggioM
                                                self.selectedH = self.appoggioH
                                        }
                                    }
                                    else
                                    {
                                        self.selectedH -= 1
                                        self.selectedM = 59
                                    }
                                }
                                else{
                                    self.selectedM -= 1
                                }
                            }
                        }
                        .frame(width: 175, height: 80)
                        HStack {
                            Picker(selection: $selectedH, label: Text("Hours")) {
                                ForEach(0 ..< secondH.count) {
                                    Text(self.secondH[$0])
                                }
                            }.frame(width: 50, height: 50, alignment: .center)
                                .disabled(flagg)
                            Text(":")
                            Picker(selection: $selectedM, label: Text("Minutes")) {
                                ForEach(0 ..< secondM.count) {
                                    Text(self.secondM[$0])
                                }
                            }.frame(width: 50, height: 50, alignment: .center)
                                .disabled(flagg)
                        }
                        Spacer()
                        if boolean == true
                        {
                            if (selectedM == 0) && (selectedH == 0)
                            {
                                Button(action: {
                                }) { Text("Start") }.disabled(true)
                                    .background(Color(#colorLiteral(red: 0, green: 0.867702961, blue: 0.4412385225, alpha: 0.3611140839)))
                                    .cornerRadius(100.0)
                                
                            }
                            else
                            {
                                Button(action: {
                                    self.mainText = "Time remaining until next reminder."
                                    self.appoggioH = self.selectedH
                                    self.appoggioM = self.selectedM
                                    self.flag = 0
                                    self.timer.connect()
                                    self.flagg = true
                                }) { Text("Start") }
                                    .background(Color(#colorLiteral(red: 0, green: 0.867702961, blue: 0.4412385225, alpha: 1)))
                                    .cornerRadius(100.0)
                                
                                
                            }                        }
                        else
                        {
                            Button(action: {
                                self.mainText = "How often would you like to be reminded to fix your posture?"
                                self.selectedM = self.appoggioM
                                self.selectedH = self.appoggioH
                                self.flag = 1
                                self.boolean = true
                                self.flagg = false
                            }) {
                                Text("Stop")
                            }.background(Color(#colorLiteral(red: 0, green: 0.867702961, blue: 0.4412385225, alpha: 1)))
                                .cornerRadius(100.0)
                        }
                    }
                }
            }
        }.navigationBarTitle(Text("Posture Fix"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(flag: 2)
    }
}
