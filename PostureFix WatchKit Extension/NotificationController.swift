//
//  NotificationController.swift
//  PostureFix WatchKit Extension
//
//  Created by Dario Mandarino on 23/01/2020.
//  Copyright © 2020 Dario Mandarino. All rights reserved.
//

import WatchKit
import SwiftUI
import UserNotifications

class NotificationController: WKUserNotificationHostingController<NotificationView> {

    override var body: NotificationView {

        return NotificationView()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func didReceive(_ notification: UNNotification) {
        // This method is called when a notification needs to be presented.
        // Implement it if you use a dynamic notification interface.
        // Populate your dynamic notification interface as quickly as possible.
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert,.sound,.badge ])
            { (status, _) in
                
                let content = UNMutableNotificationContent()
                content.title = "PostureFix"
                content.body = "Good posture breathes confidence"
                
                //Time remaining until next reminder.
                // this time interval represents the delay time of notification
                // ie., the notification will be delivered after the delay.....
                
                let request = UNNotificationRequest(identifier: "noti", content: content, trigger: .none)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                
                
                //self.alert.toggle()
                

        }

    }
}
